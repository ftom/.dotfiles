# If not running interactively, don't do anything
[ -z "$PS1" ] && return

set -o emacs

# for brew
export CXX=clang++
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.7.0_67.jdk/Contents/Home
export GOPATH=/usr/local/opt/go
# a place to put my own binaries!
export PATH=$HOME/prog/hadoop/hadoop/bin:$HOME/prog/pig/pig/bin:$HOME/bin:/Users/tfion/prog/docker/bin:/usr/local/bin:$JAVA_HOME/bin:$PATH:/usr/local/opt/go/libexec/bin:$GOPATH/bin

export HADOOP_CONF_DIR=$HOME/prog/hadoop/conf
export HADOOP_HOME=$HOME/prog/hadoop/hadoop
export PIG_HOME=$HOME/prog/pig/pig

export KAFKA_HOME=/Users/tfion/prog/scala/kafka

# use vi editing mode instead of default emacs
#set -o vi
# but don't lose C-l as the "clear the screen" key
bind -m vi-insert "\C-l":clear-screen

# append to the history file, don't overwrite it
shopt -s histappend

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

export TERM='xterm-256color' # pretty pretty colors
export HISTCONTROL=ignoreboth # don't save commands that start with space characters and don't save dupes
export HISTSIZE=1000 # keep the history file from growing gigantic
export HISTFILESIZE=2000

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# enable color output
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
fi

if [ -f $HOME/.git-prompt.sh ]; then
    source $HOME/.git-prompt.sh
fi

# advanced tab-completion for various commands in bash
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

# define some color variables that make things pretty if possible
if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    BLD=$(tput bold)
    RST=$(tput sgr0)
    RED=$BLD$(tput setaf 1)
    GRN=$BLD$(tput setaf 2)
    YLW=$BLD$(tput setaf 3)
    BLU=$BLD$(tput setaf 4)
    MAG=$(tput setaf 5)
    CYN=$(tput setaf 6)
    WHT=$BLD$(tput setaf 7)
else
    BLD=""
    RST=""
    RED=""
    GRN=""
    YLW=""
    BLU=""
    MAG=""
    CYN=""
    WHT=""
fi

# automatically say how long a command took if it ran for longer than ten seconds
function timer_start {
    timer=${timer:-$SECONDS}
}

function timer_stop {
    timer_result=$(($SECONDS - $timer))
    unset timer

    if [[ $timer_result > 60 ]]; then
        echo "${RED}>>> elapsed time ${timer_result}s"
    elif [[ $timer_result > 10 ]]; then
        echo "${YLW}>>> elapsed time ${timer_result}s"
    fi
}

trap 'timer_start' DEBUG
PROMPT_COMMAND=timer_stop

# a colorized prompt with a green or red separator depending on the success / failure of the previous command
function seperator {
    if [[ $? -eq 0 ]]; then
        COLOR=$GRN
    else
        COLOR=$RED
    fi

    echo -n $COLOR
    case $OSTYPE in
        linux-gnu*)
            printf '_%.0s' `seq 1 $COLUMNS`
        ;;
        darwin*)
            jot -s "" -b "_" $COLUMNS -n
        ;;
    esac
    echo -n $RST
}

function display_cryptenv {
    if [[ ! -z $CRYPTENV ]]; then
        echo -n ${BLD}${RED}cryptenv:$CRYPTENV${RST}
    fi
}

export GIT_PS1_SHOWDIRTYSTATE=1 # show * and + when repository is dirty
export PS1='$(seperator)\n${MAG}\u${RST}@${BLU}\h${RST} in ${WHT}\w$(__git_ps1 "${YLW} on branch %s")${RST} $(display_cryptenv)\n\$ '

function use_license {
    rm -f $TALEND_STUDIO_LICENSE
    ln -sf $1 $TALEND_STUDIO_LICENSE
}

function use_build {
    rm -f $TALEND_STUDIO_HOME
    ln -sf $1 $TALEND_STUDIO_HOME
}

alias s='source $HOME/.bash_profile'

export TALEND_STUDIO_LICENSE=/Users/tfion/prog/builds/latest_license
export TALEND_STUDIO_HOME=/Users/tfion/prog/builds/latest_build

alias talend_build='$TALEND_STUDIO_HOME/Talend-Studio-macosx-cocoa.app/Contents/MacOS/Talend-Studio-macosx-cocoa -data $TALEND_STUDIO_HOME/workspace -vmargs -Xms64m -Xmx768m -XstartOnFirstThread -Dorg.eclipse.swt.internal.carbon.smallFonts'

alias refresh_storm_lib_jar='cp $HOME/prog/repositories/tbd-studio-ee/main/components_libs/talend-storm-lib/target/talend-storm-lib-5.6.0-SNAPSHOT.jar $HOME/prog/repositories/tbd-studio-ee/main/plugins/org.talend.designer.components.stormprovider/components/tStormConfiguration/talend-storm-lib-5.6.0-SNAPSHOT.jar'

export DOCKER_HOST=tcp://192.168.59.103:2376
export DOCKER_CERT_PATH=/Users/tfion/.boot2docker/certs/boot2docker-vm
export DOCKER_TLS_VERIFY=1

function setenv_docker {
    export DOCKER_HOST=tcp://192.168.59.103:2376
    export DOCKER_CERT_PATH=/Users/tfion/.boot2docker/certs/boot2docker-vm
    export DOCKER_TLS_VERIFY=1
}

function setenv_weave {
    export DOCKER_HOST=tcp://192.168.59.103:12375
    export DOCKER_CERT_PATH=
    export DOCKER_TLS_VERIFY=
}

export DOCKER_IP=192.168.59.103

alias mongod2_6='docker run -v /Users/tfion/prog/data/mongodb2.6:/data -p 27017:27017 -d mongo:2.6.4'
alias mongod2_4='docker run -v /Users/tfion/prog/data/mongodb2.4:/data -p 29017:27017 -d mongo:2.4.10'
alias cassandra='docker run -p 9160:9160 -p 9042:9042 -d spotify/cassandra:latest'
alias mysql='docker run -p 3306:3306 -e MYSQL_ROOT_PASSWORD=first1 -d --name mysql mysql:latest'
alias restart_dns='sudo killall -HUP mDNSResponder'
alias rm='rm -i'
alias cp='cp -i'
alias clean_docker='docker ps -a -q -f status=exited | xargs docker rm'
alias clean_images='docker rmi -f $(docker images -q -a)'
alias xd='cd ..'
alias ml='ls -tlrah'

function fjava {
    find . -name \*.java -exec grep $@ {} \; -print
}

function fxml {
    find . -name \*_java.xml -exec grep $@ {} \; -print
}

function fjet {
    find . -name \*.javajet -exec grep $@ {} \; -print
}

function docker-enter {
    boot2docker ssh '[ -f /var/lib/boot2docker/nsenter ] || docker run --rm -v /var/lib/boot2docker/:/target jpetazzo/nsenter'
    boot2docker ssh -t sudo /var/lib/boot2docker/docker-enter "$@"
}

function ipd {
    docker inspect -f '{{ .NetworkSettings.IPAddress }}' "$@"
}

function subl {
    /Applications/Sublime\ Text.app/Contents/MacOS/Sublime\ Text -a "$@"
}

function restart_dnsmasq {
    sudo launchctl stop homebrew.mxcl.dnsmasq
    sudo launchctl start homebrew.mxcl.dnsmasq
}

# load host-local configurations
if [ -f ~/.bashrc.local ]; then
    . ~/.bashrc.local
fi

alias hls="hadoop fs -ls"
alias hdu="hadoop fs -du -h"
alias hcat="hadoop fs -text"
alias hget="hadoop fs -get"
alias hrm="hadoop fs -rm -r"
alias flush_kafka="kafka-topics.sh --zookeeper zookeeper1.weave.local:2181,zookeeper2.weave.local:2181 --topic hellweek --alter --config retention.ms=10000"

# complete -W "$(grep '^Host' ~/.ssh/config | grep -o '\w\+$')" ssh

if [ -f $(brew --prefix)/etc/bash_completion ]; then
    . $(brew --prefix)/etc/bash_completion
fi

